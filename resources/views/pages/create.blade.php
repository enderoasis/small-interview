@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12 ml-4">
            <div class="panel panel-default">
                <div class="panel-heading ml-3 text-center"><b>Обратная связь</b></div>

                <div class="panel-body">
                    <br>
                    <form class="form-horizontal" enctype="multipart/form-data" action="/callbacks" style="margin-left: 700px;" role="form" method="POST">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label for="subject" class="col-md-4 control-label">Тема</label>

                            <div class="col-md-6">
                                <input id="subject" type="text" class="form-control" name="subject" value="{{ old('subject') }}" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="content" class="col-md-4 control-label">Содержание</label>

                            <div class="col-md-6">
                                <textarea rows="5" id="content" class="form-control" name="content" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="custom-file">
                                      <input type="file" class="custom-file" name="files[]" multiple="true">
                                 </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn"></i> Создать заявку
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection