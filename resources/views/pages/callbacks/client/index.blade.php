@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 ml-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="{{ url('create-callback') }}" class="btn btn-primary">Создать заявку</a>
                    <hr>
                    @if ($callbacks->isEmpty())
                        <center><h2>Нет активных заявок</h2></center>
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Идентификатор заявки</th>
                                <th>Тема обращения</th>
                                <th>Статус</th>
                                <th>Последнее обновление</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($callbacks as $callback)
                                <tr>
                                    <td>
                                        Заявка #{{ $callback->id }}
                                    </td>
                                    <td>
                                        
                                            #{{ $callback->id }} - {{ $callback->subject }}
                                        
                                    </td>
                                    <td>
                                        @if ($callback->status === 'open')
                                            <span class="label label-success">{{ $callback->status }}</span>
                                        @else
                                            <span class="label label-danger">{{ $callback->status }}</span>
                                        @endif
                                    </td>
                                    <td>{{ $callback->updated_at }}</td>
                                    <td>
                                            <a href="{{ url('callbacks/' . $callback->id) }}" class="btn btn-primary">Посмотреть</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$callbacks->links('pagination::bootstrap-4')}}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection