<div class="panel panel-default">
    <div class="panel-heading">Ответить</div>

        <div class="panel-body">
            <div class="comment-form">

                <form action="{{ url('comment') }}" method="POST" class="form">
                    {!! csrf_field() !!}

                    <input type="hidden" name="callback_id" value="{{ $callback->id }}">

                    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                        <textarea rows="5" id="comment" class="form-control" name="comment"></textarea>

                        @if ($errors->has('comment'))
                            <span class="help-block">
                               <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
</div>