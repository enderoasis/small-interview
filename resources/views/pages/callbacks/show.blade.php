@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-8 ml-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Тема: {{ $callback->subject }}
                </div>

                <div class="panel-body">
                    
                    <div class="callback-info">
                        <p>Сообщение: {{ $callback->content }}</p>
                         <p>
                            @if ($callback->status === 'open')
                                Статус: <span class="label label-success">{{ $callback->status }}</span>
                            @else
                                Статус: <span class="label label-danger">{{ $callback->status }}</span>
                            @endif
                        </p>
                        <p>Файлы</p>
                        <br>
                    @if(!empty(json_decode($callback->files)))
                        @foreach(json_decode($callback->files) as $file)
                             <a target="blank_" href="{{ url('/docs/'.$file)}}">{{$file}}</a><br>
                        @endforeach
                    @endif
                    <br>
                    <p>Создано: {{ $callback->created_at->diffForHumans() }}</p>
                    </div>

                </div>
            </div>
            <hr>
            <br>
            @include('pages.callbacks.comments')
            
            <br>
            @include('pages.callbacks.reply')
            

        </div>
    </div>


@endsection