@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                

                <div class="panel-body">
                    @if ($callbacks->isEmpty())
                        <p>Нет активных заявок, компания вами гордится!</p>
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Идентификатор заявки</th>
                                <th>Тема обращения</th>
                                <th>Имя клиента</th>
                                <th>Почта клиента</th>
                                <th>Статус</th>
                                <th>Время создания</th>
                                <th>Последнее обновление</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($callbacks as $callback)
                                <tr>
                                    <td>
                                        Заявка #{{ $callback->id }}
                                    </td>
                                    <td>
                                        
                                            #{{ $callback->id }} - {{ $callback->subject }}
                                        
                                    </td>
                                    <td>{{$callback->user->name}}</td>
                                    <td><a href="mailto:{{$callback->user->email}}">{{$callback->user->email}}</a></td>
                                    <td>
                                        @if ($callback->status === 'open')
                                            <span class="label label-success">{{ $callback->status }}</span>
                                        @else
                                            <span class="label label-danger">{{ $callback->status }}</span>
                                        @endif
                                    </td>
                                    <td>{{$callback->created_at}}</td>
                                    <td>{{ $callback->updated_at }}</td>
                                    <td>
                                        @if($callback->status === 'open')
                                            <a href="{{ url('callbacks/' . $callback->id) }}" class="btn btn-primary">Ответить</a>  

                                            <form action="{{ url('/callbacks/close/' . $callback->id) }}" method="POST">
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-danger">Закрыть</button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$callbacks->links('pagination::bootstrap-4')}}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection