<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Callback;

class CallbackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,3) as $index) {
	        Callback::create([
	            'subject' => $faker->catchPhrase,
	            'content' => $faker->paragraph,
	            'status' => 'open',
	       		'user_id' => 2
	        ]);
      }
    }
}
