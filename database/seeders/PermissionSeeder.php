<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manageCallback = new Permission();
        $manageCallback->name = 'Manage callbacks';
        $manageCallback->slug = 'manage-callback';
        $manageCallback->save();


        $createCallback = new Permission();
        $createCallback->name = 'Create Callbacks';
        $createCallback->slug = 'create-callback';
        $createCallback->save();
    }
}
