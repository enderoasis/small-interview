<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use App\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manager_role = Role::where('slug', 'manager')->first();
        $manageCallbacks = Permission::where('slug','manage-callback')->first();

        $manager = new User();
        $manager->name = 'Менеджер';
        $manager->email = 'manager@small.kz';
        $manager->password = bcrypt('manager2021');
        $manager->save();
        $manager->roles()->attach($manager_role);
        $manager->permissions()->attach($manageCallbacks);

        $client_role = Role::where('slug', 'client')->first();
        $createCallbacks = Permission::where('slug','create-callback')->first();

        $client = new User();
        $client->name = 'Клиент';
        $client->email = 'client@small.kz';
        $client->password = bcrypt('client2021');
        $client->save();
        $client->roles()->attach($client_role);
        $client->permissions()->attach($createCallbacks);

    }
}
