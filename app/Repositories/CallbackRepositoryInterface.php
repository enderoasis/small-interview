<?php

namespace App\Repositories;

interface CallbackRepositoryInterface
{

    public function get($id);

    public function all();

    public function store($data);

    public function close($callback_id);
}
