<?php

namespace App\Repositories;

use App\Models\Comment;
use Auth;

class CommentRepository 

{
	public function store($data)
	    {
	      
	        $comment = Comment::create(
	            [    
	            'callback_id' => $data['callback_id'],
	            'user_id' => Auth::user()->id,
	            'comment' => $data['comment']
	            ]
	        );

	    return redirect()->back()->with("status", "Ваше обращение отправлено.");
	    }
}