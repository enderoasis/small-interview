<?php

namespace App\Repositories;

use App\Models\Callback;
use File;
use Auth;
use Carbon\Carbon;

class CallbackRepository implements CallbackRepositoryInterface
{
 
    public function get($id)
    {
        return Callback::find($id);
    }

    public function all()
    {
        return Callback::orderby('created_at','desc')->paginate(5);
    }

    public function store($data)
    {

         if (isset($data['files'])) 
         {
            foreach ($data['files'] as $file) 
            {
                $name = $file->getClientOriginalName();
                $file->move(public_path() . '/docs/', $name);
                $file_names[] = $name;
                //return json_encode($file_names);
            }
                // Оставление заявки не чаще чем 1 раз в сутки
         }
         else {
            $file_names = null;
         }
 
          if(Auth::user()->callbacks()->whereDate('created_at', Carbon::today())->count() < 1)
          {
                $callback = Callback::create(
                [   
                 'subject' => $data['subject'],
                 'content' => $data['content'],
                 'files' => json_encode($file_names),
                 'user_id' => Auth::user()->id
                ]);

                return redirect()->to('home');
           }
           else 
           {
                return "Вы не можете оставлять заявки более 1 раза в сутки. Спасибо за понимание";
           }
 
    }

    public function close($callback_id)
    {
        $callback = Callback::findorfail($callback_id);
        
        $callback_upd = $callback->update(
            [
             'status' => 'close'
            ]
        );

        return redirect()->back();
    }
}