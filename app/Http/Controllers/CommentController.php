<?php

namespace App\Http\Controllers;

use App\Repositories\CommentRepository;
use Illuminate\Http\Request;

class CommentController extends Controller
{
	protected $comment;

	public function __construct(CommentRepository $comment)
    {
        $this->comment = $comment;
    }

    public function store(Request $request)
    {
    	$data = $request->all();
        return $this->comment->store($data);
    }
}
