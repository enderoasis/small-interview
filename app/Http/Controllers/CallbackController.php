<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CallbackRepositoryInterface;
use Auth;

class CallbackController extends Controller
{
	protected $callback;

	public function __construct(CallbackRepositoryInterface $callback)
    {
        $this->callback = $callback;
    }

    public function index()
    {
        $user_role = Auth::user()->roles()->first();
        
        if ($user_role)
        {
            if ($user_role->first()->slug = 'manager')
            {
                // Для менеджера должны быть доступны все заявки
                return view('pages.callbacks.admin.index', ['callbacks' => $this->callback->all()]);
            }
        }
        else
            {
                // Другим только их собственные
                return view('pages.callbacks.client.index', ['callbacks' => Auth::user()->callbacks()->paginate(10)]);   
            }
    }

    public function displayForm()
    {
        return view('pages.create');
    }

    public function store(Request $request)
    {
    	$data = $request->all();
        return $this->callback->store($data);
    }

    public function close()
    {
        $callback_id = request('id');
        $this->callback->close($callback_id);   
    }
    
    public function show($id)
    {
       return view('pages.callbacks.show',['callback' => $this->callback->get($id)]);
    }
    
}
