<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Callback extends Model
{
    use HasFactory;

    protected $fillable = ['subject','content','user_id','status','files'];
    
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    // Для определения пользователя создавшего Callback
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
