<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['callback_id','user_id','comment'];

    public function callback()
    {
        return $this->belongsTo(Callback::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
