# small-interview

Small tech interview

Запустить указанные команды:

1) php artisan key:generate
2) Создать базу данных в mysql, настроить .env указать логин пароль к бд.
3) php artisan migrate
4) php artisan db:seed
5) php artisan serve
6) Перейти по адресу http://localhost:8000

# Доступы

По требованию задания аккаунт менеджера создан заранее:

Login: manager@small.kz <br>
Password: manager2021  

Для удобства сгенерированы фейковые данные для заявок.
