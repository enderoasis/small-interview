<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\CallbackController::class, 'index']);
Route::get('/callbacks/{id}', [App\Http\Controllers\CallbackController::class, 'show']);
Route::post('/callbacks', [App\Http\Controllers\CallbackController::class, 'store']);
Route::post('/callbacks/close/{id}', [App\Http\Controllers\CallbackController::class, 'close']);
Route::post('/comment', [App\Http\Controllers\CommentController::class, 'store']);
Route::get('/create-callback', [App\Http\Controllers\CallbackController::class, 'displayForm']);

